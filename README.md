# ulisp-st7735r

A uLisp library for a Sitronix ST7735R TFT LCD driver chip.

Note there are differences between a '35 and a '35R, such as PWCTR1,
such that this will not work for a ST7735

The uLisp language home page is at http://www.ulisp.com

This I2C chip is used, for example, to control a TFT LCD display on
an Adafruit (tm) Arduino (tm) shield.

See the project wiki page at

https://gitlab.com/SpringCitySolutionsLLC/ulisp-st7735r/wikis/home

#
