; level2.lisp
; (C) 2019-2020 Spring City Solutions LLC
; MIT License, see LICENSE file

; The ST7735R driver has four design levels

; Level 0 Direct hardware control
; If your board has different pinouts, or I2C expanders,
; you can edit this file to match your hardware.

; Level 1 Interface mode support
; The driver currently only implements 4-wire serial mode.
; If you wired up one of the numerous other interface modes
; such as one of the numerous parallel modes or 3-wire
; serial, you could implement the interface mode functions here.

; Level 2 Low level chip control
; Sending raw commands to the ST7735R
; For example, the ST7735R chip PTLON command is command code 0x12
; regardless of specific interface mode or hardware wiring.

; Level 3 High level chip commands
; The user level API.
; Production end users should only call functions found in level3.lisp.

; The project standard command names come from the Sitronix ST7735R datasheet.

; Datasheet 10.1.1 NOP (00h): Does nothing

(defun st7735r:command-nop nil
  (st7735r:write-command #x00 '())
  (delay 5)
  (eval t)
  )

; Datasheet 10.1.2 SWRESET (01h): Software Reset
; Loads default values into registers.  Takes 120 ms
; Example: (st7735r:command-swreset)

(defun st7735r:command-swreset nil
  (st7735r:write-command #x01 '())
  (delay 120)
  (eval t)
  )

; Datasheet 10.1.10 SLPIN (10h): Sleep In
; Display blank screen, stop all oscillators and scanning.
; Does NOT erase frame memory.
; Takes 120 ms.
; Can only exit via SLPOUT command

(defun st7735r:command-slpin nil
  (st7735r:write-command #x10 '())
  (delay 120)
  (eval t)
  )

; Datasheet 10.1.11 SLPOUT (11h): Sleep Out
; Start oscillators and scanning, display frame memory on screen.
; Does NOT erase frame memory.
; Takes 120 ms.
; Only way to exit SLPIN command

(defun st7735r:command-slpout nil
  (st7735r:write-command #x11 '())
  (delay 120)
  (eval t)
  )

; Datasheet 10.1.12 PTLON (12h): Partial Display Mode On
; Opposite of NORON command

(defun st7735r:command-ptlon nil
  (st7735r:write-command #x12 '())
  (delay 10)
  (eval t)
  )

; Datasheet 10.1.13 NORON (13h): Normal Display Mode On
; Opposite of PTLON command

(defun st7735r:command-noron nil
  (st7735r:write-command #x13 '())
  (delay 10)
  (eval t)
  )

; Datasheet 10.1.14 INVOFF (20h): Display Inversion Off
; Cancels INVON, normal conversion of framebuffer to RGB values

(defun st7735r:command-invoff nil
  (st7735r:write-command #x20 '())
  (delay 5)
  (eval t)
  )

; Datasheet 10.1.15 INVON (21h): Display Inversion On
; Instantly inverts RGB values displayed, no effect on framebuffer contents

(defun st7735r:command-invon nil
  (st7735r:write-command #x21 '())
  (delay 5)
  (eval t)
  )

; Datasheet 10.1.17 DISPOFF (28h): Display Off
; No effect on power or stored data merely displays a blank screen instead of frame buffer

(defun st7735r:command-dispoff nil
  (st7735r:write-command #x28 '())
  (delay 120)
  (eval t)
  )

; Datasheet 10.1.18 DISPON (29h): Display On
; Cancels DISPOFF and displays framebuffer instead of blank screen

(defun st7735r:command-dispon nil
  (st7735r:write-command #x29 '())
  (delay 120)
  (eval t)
  )

; Datasheet 10.1.19 CASET (2ah): Column Address Set
; Full screen for a green tab LCD is xstart 2 xend 129

(defun st7735r:command-caset (start-msb start-lsb end-msb end-lsb)
  (st7735r:write-command #x2a (list start-msb start-lsb end-msb end-lsb))
  (eval t)
  )

; Datasheet 10.1.20 RASET (2Bh): Row Address Set
; Full screen for a green tab LCD is xstart 1 xend 160

(defun st7735r:command-raset (start-msb start-lsb end-msb end-lsb)
  (st7735r:write-command #x2b (list start-msb start-lsb end-msb end-lsb))
  (eval t)
  )

; Datasheet 10.1.21 RAMWR (2Ch): Memory Write

(defun st7735r:command-ramwr (red green blue)
  (st7735r:write-command #x2c (list red green blue))
  (eval t)
  )

; Datasheet 10.1.27 MADCTL (36h): Memory Data Access Control
; Mostly controls scan direction of frame buffer to LCD panel

(defun st7735r:command-madctl (my mx mv ml rgb mh)
  (let ((my-p 0)
        (mx-p 0)
        (mv-p 0)
        (ml-p 0)
        (rgb-p 0)
        (mh-p 0))
    (cond ((eq my 'MY) (setq my-p 128)))
    (cond ((eq mx 'MX) (setq mx-p 64)))
    (cond ((eq mv 'MV) (setq mv-p 32)))
    (cond ((eq ml 'Bottom-To-Top) (setq ml-p 16)))
    (cond ((eq rgb 'BGR) (setq rgb-p 8)))
    (cond ((eq mh 'Right-To-Left) (setq mh-p 4)))
    (st7735r:write-command #x36
                           (list (+ my-p mx-p mv-p ml-p rgb-p mh-p)))
    (delay 1)
    (eval t)
    )
  )

; Datasheet 10.1.30 COLMOD (3Ah): Interface Pixel Format

(defun st7735r:command-colmod (bitcolor)
  (let (ifpf #x05)
    (cond
      ((eq bitcolor '12bitcolor) (setq ifpf #x03))
      ((eq bitcolor '16bitcolor) (setq ifpf #x05))
      ((eq bitcolor '18bitcolor) (setq ifpf #x06))
      )
    (st7735r:write-command #x3a (list ifpf))
    (delay 1)
    (eval t)
    )
  )

; Datasheet 10.2.1 FRMCTR1 (B1h): Frame Rate Control in Full color mode
; Configure vertical frame rate oscillators.
; The screen refresh rate in Hz is the horizontal clock fosc in Hz
; divided by the number of clock cycles in a complete frame
; The front porch is extra lines at the bottom of the LCD
; The back porch is extra lines at the top of the LCD
; The RTN seems to be a clocks per line value used later
; in a  PLL to generate the horizontal dot clock.
; Frame rate=fosc/((RTNA x 2 + 40) x (LINE + FPA + BPA + 2))
; fosc = 850kHz

(defun st7735r:command-frmctr1 (rtna fpa bpa)
  (st7735r:write-command #xb1 (list rtna fpa bpa))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.2 FRMCTR2 (B2h): Frame Rate Control in Idle mode 8-colors
; Configure vertical oscillators.
; The screen refresh rate in Hz is the horizontal clock fosc in Hz
; divided by the number of clock cycles in a complete frame
; The front porch is extra lines at the bottom of the LCD
; The back porch is extra lines at the top of the LCD
; The RTN seems to be a clocks per line value used later
; in a  PLL to generate the horizontal dot clock.
; Frame rate=fosc/((RTNB x 2 + 40) x (LINE + FPA + BPA +2))
; fosc = 850kHz

(defun st7735r:command-frmctr2 (rtnb fpa bpa)
  (st7735r:write-command #xb2 (list rtnb fpa bpa))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.3 FRMCTR3 (B3h): Frame Rate Control in Partial mode full color
; Configure vertical oscillators.
; The screen refresh rate in Hz is the horizontal clock fosc in Hz
; divided by the number of clock cycles in a complete frame
; The front porch is extra lines at the bottom of the LCD
; The back porch is extra lines at the top of the LCD
; The RTN seems to be a clocks per line value used later
; in a  PLL to generate the horizontal dot clock.
; C = dot inversion mode
; Frame rate=fosc/((RTNC x 2 + 40) x (LINE + FPA + BPA +2))
; fosc = 850kHz
; D = column inversion mode
; Frame rate=fosc/((RTND x 2 + 40) x (LINE + FPA + BPA +2))
; fosc = 850kHz

(defun st7735r:command-frmctr3 (rtnc fpa-line bpa-line rtnd fpa-frame bpa-frame)
  (st7735r:write-command #xb3 (list rtnc fpa-line bpa-frame rtnd fpa-frame bpa-frame))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.4 INVCTR (B4h): Display Inversion Control
; parameters are display modes, column or dot mode

(defun st7735r:command-invctr (full idle partial)
  (let ((full-p 0) (idle-p 0) (partial-p 0))
    (cond ((eq full 'column) (setq full-p 1)))
    (cond ((eq idle 'column) (setq idle-p 1)))
    (cond ((eq partial 'column) (setq partial-p 1)))
    (st7735r:write-command #xb4 (list (+ (* full-p 4) (* idle-p 2) (* partial-p 1))))
    (delay 1)
    (eval t)
    )
  )

; Datasheet 10.2.6 PWCTR1 (C0h): Power Control 1

(defun st7735r:command-pwctr1 (AVdd GVdd GVcl Mult)
  (let ((byte1 #xa0)
        (byte2 #x02)
        (byte3 #x84)
        (VRHP #x0)
        (VRHN #x0))
    (cond ((= AVdd 4.5) (setq byte1 #x00)))
    (cond ((= AVdd 4.6) (setq byte1 #x20)))
    (cond ((= AVdd 4.7) (setq byte1 #x40)))
    (cond ((= AVdd 4.8) (setq byte1 #x60)))
    (cond ((= AVdd 4.9) (setq byte1 #x80)))
    (cond ((= AVdd 5.0) (setq byte1 #xa0)))
    (cond ((= AVdd 5.1) (setq byte1 #xc0)))
    (setq VRHP (round (* (- 4.7 GVdd) 20)))
    (cond
      ((< VRHP 0)(setq VRHP 0))
      ((< 31 VRHP)(setq VRHP 31))
      )
    (setq byte1 (+ byte1 VRHP))
    (setq VRHN (round (* (+ 4.7 GVcl) 20)))
    (cond
      ((< VRHN 0)(setq VRHN 0))
      ((< 31 VRHN)(setq VRHN 31))
      )
    (setq byte2 (+ byte2 VRHN))
    (cond ((eq Mult '2x) (setq byte3 #x04)))
    (cond ((eq Mult '3x) (setq byte3 #x44)))
    (cond ((eq Mult 'Auto) (setq byte3 #x84)))
    (st7735r:write-command #xc0 (list byte1 byte2 byte3))
    (delay 1)
    (eval t)
    )
  )

; Datasheet 10.2.7 PWCTR2 (C1h): Power Control 2

(defun st7735r:command-pwctr2 (vgh-vgl)
  (st7735r:write-command #xc1 (list vgh-vgl))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.8 PWCTR3 (C2h): Power Control 3 opamp and boost clock (Normal mode Full color)

(defun st7735r:command-pwctr3 (ap dc)
  (st7735r:write-command #xc2 (list ap dc))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.9 PWCTR4 (C3h): Power Control 4 (in Idle mode/ 8-colors)

(defun st7735r:command-pwctr4 (ap dc)
  (st7735r:write-command #xc3 (list ap dc))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.10 PWCTR5 (C4h): Power Control 5 (in Partial mode/ full-colors)
; TODO: Make configurable.

(defun st7735r:command-pwctr5 (ap dc)
  (st7735r:write-command #xc4 (list ap dc))
  (delay 1)
  (eval t)
  )

; Datasheet 10.2.11 VMCTR1 (C5h): VCOM Control 1
; Sets the Vcom voltage for the LCD

(defun st7735r:command-vmctr1 (vcom)
  (let (vcoms 0)
    (setq vcoms (round (* -40 (+ vcom .425))))
    (cond
      ((< vcoms 0)(setq vcoms 0))
      ((< 63 vcoms)(setq vcoms 63))
      )
    (st7735r:write-command #xc5 (list vcoms))
    (delay 1)
    (eval t)
    )
  )

; Datasheet 10.2.19 GMCTRP1 (E0): Gamma Positive Correction Setting
; TODO: Make configurable.  Default is ... ?

(defun st7735r:command-gmctrp1 nil
  (st7735r:write-command #xe0 '(
                               #x02 #x1c #x07 #x12
                               #x37 #x32 #x29 #x2d
                               #x29 #x25 #x2b #x39
                               #x00 #x01 #x03 #x10))
  (delay 10)
  (eval t)
  )

; Datasheet 10.2.20 GMCTRN1 (E1): Gamma Negative Correction Setting
; TODO: Make configurable.  Default is ... ?

(defun st7735r:command-gmctrn1 nil
  (st7735r:write-command #xe1 '(
                               #x03 #x1d #x07 #x06
                               #x2e #x2c #x29 #x2d
                               #x2e #x2e #x37 #x3f
                               #x00 #x00 #x02 #x10))
  (delay 10)
  (eval t)
  )

; end of level2.lisp.example file
